# React Side Componet 

This side-component is specifically for [react-main-component](https://gitlab.com/zaplib/react-main-component)

## Install

```
git clone https://gitlab.com/zaplib/react-side-component.git
cd react-side-component
npm install
```

## Pack Compoent

```
npm run module
npm run stylus
```

## ENV Version

* **Node.js: v9.7.1**
* **npm: 5.6.0**


## Package Version

* **react: 16**
* **redux: 4** 
* **react-router-dom: 4** 
* **connected-react-router: 6** (棄用react-router-redux)
* **postcss-loader: 3**

## Dev Package Version

* **babel: 7**
* **webpack: 4**
