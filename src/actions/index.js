const action = (type, payload = {}) => ({ type, ...payload })

export const READYLOG = 'READYLOG'
export const TESTLOG = 'TESTLOG'

export const readylog = () => action(READYLOG)
export const testlog = (data) => action(TESTLOG,data)