import { connect } from "react-redux"
import Test from '../components/SideDiv'
import *  as actions from '../actions'

const mapStateToProps =(state) =>{
    const {test} = state
    return {test}
}

const mapDispatchToProps = (dispatch) =>{
    return {
        readylog : ()=>dispatch(actions.readylog())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Test)



