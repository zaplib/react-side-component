import { fork } from 'redux-saga/effects'
import * as test from './test.js'

export default function* () {
    yield fork(test.testLog)
}