import { take, call, put, select } from 'redux-saga/effects'
import * as actions from '../actions'

export function* testLog() {
    while (true) {
        yield take(actions.READYLOG)
        yield put(actions.testlog({ msg: new Date().toString() }))
    }
}