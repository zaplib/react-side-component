import {
    TESTLOG
} from '../actions'



export default function (state = {
    msg: 'init'
}, action) {
    switch (action.type) {
        case TESTLOG:
            return {
                ...state,
                msg: action.msg
            }
        default: return state
    }
}

