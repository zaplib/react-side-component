import React, { Component } from 'react'
//import { hot } from 'react-hot-loader'
import Test from '../containers/Test'
import SideDiv from '../containers/SideDiv'
class App extends Component {
    render() {
        return (
            <div>
                <Test/>
                <SideDiv/>
            </div>
        )
    }
}

//export default hot(module)(App);
export default App;