"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = configureStore;
exports.history = void 0;

var _redux = require("redux");

var _reduxSaga = _interopRequireDefault(require("redux-saga"));

var _history = require("history");

var _connectedReactRouter = require("connected-react-router");

var _reducers = _interopRequireDefault(require("../reducers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var sagaMiddleware = (0, _reduxSaga["default"])();
var history = (0, _history.createBrowserHistory)();
exports.history = history;

function configureStore(initialState) {
  var middlewarelist = [(0, _connectedReactRouter.routerMiddleware)(history), sagaMiddleware];

  if (process.env.NODE_ENV !== 'production') {
    var Logger = require("redux-logger");

    middlewarelist = [].concat(_toConsumableArray(middlewarelist), [Logger.createLogger()]);
  }

  return _objectSpread({}, (0, _redux.createStore)((0, _reducers["default"])(history), initialState, _redux.applyMiddleware.apply(void 0, _toConsumableArray(middlewarelist))), {
    runSaga: sagaMiddleware.run
  });
}