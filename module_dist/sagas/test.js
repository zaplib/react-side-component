"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.testLog = testLog;

var _effects = require("redux-saga/effects");

var actions = _interopRequireWildcard(require("../actions"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(testLog);

function testLog() {
  return regeneratorRuntime.wrap(function testLog$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (!true) {
            _context.next = 7;
            break;
          }

          _context.next = 3;
          return (0, _effects.take)(actions.READYLOG);

        case 3:
          _context.next = 5;
          return (0, _effects.put)(actions.testlog({
            msg: new Date().toString()
          }));

        case 5:
          _context.next = 0;
          break;

        case 7:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}