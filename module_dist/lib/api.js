"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = _default;

var _querystring = _interopRequireDefault(require("querystring"));

var _isomorphicFetch = _interopRequireDefault(require("isomorphic-fetch"));

var _Config = require("Config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _default(_ref) {
  var _ref$api = _ref.api,
      api = _ref$api === void 0 ? api : _ref$api,
      cmd = _ref.cmd,
      _ref$method = _ref.method,
      method = _ref$method === void 0 ? 'GET' : _ref$method,
      _ref$type = _ref.type,
      type = _ref$type === void 0 ? 'json' : _ref$type,
      _ref$data = _ref.data,
      data = _ref$data === void 0 ? {} : _ref$data,
      _ref$header = _ref.header,
      header = _ref$header === void 0 ? {} : _ref$header,
      _ref$fileList = _ref.fileList,
      fileList = _ref$fileList === void 0 ? false : _ref$fileList,
      _ref$cors = _ref.cors,
      cors = _ref$cors === void 0 ? true : _ref$cors;
  method = method.toUpperCase();
  type = type.toLowerCase();

  var option = {
    method: method,
    headers: _objectSpread({
      'Accept': 'application/javascript'
    }, header),
    credentials: cors ? 'include' : 'same-origin'
  },
      dataStr = _querystring["default"].stringify(data);

  if (method == 'GET' || method == 'PATCH') {
    cmd += "?" + dataStr;
  } else if (method == 'DELETE') {
    option.body = dataStr;
  } else {
    var formData = new FormData();

    if (fileList) {
      formData.append('file', fileList[0], fileList[0].name);

      for (var k in data) {
        formData.append(k, data[k]);
      }
    }

    option.body = fileList ? formData : dataStr;
    if (!fileList) option.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  var url = cmd,
      tmp_res = {};
  return (0, _isomorphicFetch["default"])(url, option).then(function (res) {
    var data = res.ok ? type == 'json' ? res.json() : res.text() : null;
    tmp_res = {
      ok: res.ok,
      status: res.status,
      statusText: res.statusText,
      type: res.type,
      url: res.url,
      redirected: res.redirected,
      bodyUsed: res.bodyUsed,
      headers: res.headers,
      body: data
    };
    return data;
  }).then(function (body) {
    return _objectSpread({}, tmp_res, {
      body: body
    });
  })["catch"](function (err) {
    var resp = {
      ok: false,
      url: url,
      body: null,
      statusText: err,
      status: null
    };
    return resp;
  });
}