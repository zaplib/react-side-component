"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.testlog = exports.readylog = exports.TESTLOG = exports.READYLOG = void 0;

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var action = function action(type) {
  var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return _objectSpread({
    type: type
  }, payload);
};

var READYLOG = 'READYLOG';
exports.READYLOG = READYLOG;
var TESTLOG = 'TESTLOG';
exports.TESTLOG = TESTLOG;

var readylog = function readylog() {
  return action(READYLOG);
};

exports.readylog = readylog;

var testlog = function testlog(data) {
  return action(TESTLOG, data);
};

exports.testlog = testlog;