"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _redux = require("redux");

var _connectedReactRouter = require("connected-react-router");

var _test = _interopRequireDefault(require("./test"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(history) {
  return (0, _redux.combineReducers)({
    test: _test["default"],
    router: (0, _connectedReactRouter.connectRouter)(history)
  });
};

exports["default"] = _default;