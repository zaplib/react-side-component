"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _reactRouterDom = require("react-router-dom");

var _connectedReactRouter = require("connected-react-router");

var _reactRedux = require("react-redux");

require("./styl/index.styl");

var _store = _interopRequireWildcard(require("./store"));

var _sagas = _interopRequireDefault(require("./sagas"));

var _App = _interopRequireDefault(require("./components/App"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var store = (0, _store["default"])();
store.runSaga(_sagas["default"]);

var _default = _react["default"].createElement(_reactRedux.Provider, {
  store: store
}, _react["default"].createElement(_connectedReactRouter.ConnectedRouter, {
  history: _store.history
}, _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement(_reactRouterDom.Switch, null, _react["default"].createElement(_reactRouterDom.Route, {
  exact: true,
  path: "/side",
  component: _App["default"]
})))));
/*
render(
    
    , document.getElementById('root')
)
*/


exports["default"] = _default;