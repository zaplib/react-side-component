"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _root = _interopRequireDefault(require("./root"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

(0, _reactDom.render)(_root["default"], document.getElementById('root'));