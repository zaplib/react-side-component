"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SideDiv", {
  enumerable: true,
  get: function get() {
    return _SideDiv["default"];
  }
});

var _SideDiv = _interopRequireDefault(require("./components/SideDiv"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }